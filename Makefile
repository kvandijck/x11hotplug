PROGS	= x11hotplug
default: $(PROGS)

LOCALVERSION	:= $(shell ./getlocalversion .)

PREFIX	= /usr/local
CFLAGS	= -Wall -g3 -O0
CPPFLAGS= -D_GNU_SOURCE
LDLIBS	=
INSTOPTS= -s

-include config.mk

%: %.c
	@echo "LCC $*"
	@$(CC) -o $@ -DLOCALVERSION=\"$(LOCALVERSION)\" -DNAME=\"$*\" $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) $^ $(LDLIBS)

%.o: %.c
	@echo " CC $*"
	@$(CC) -c $@ $(CPPFLAGS) $(CFLAGS) $^

clean:
	rm -f $(wildcard *.o) $(PROGS)

install: $(PROGS)
	install -v $(INSTOPTS) $^ $(DESTDIR)$(PREFIX)/bin
