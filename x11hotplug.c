#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <unistd.h>
#include <error.h>
#include <getopt.h>
#include <sys/socket.h>
#include <sys/un.h>

/* ARGUMENTS */
static const char help_msg[] =
	NAME ": issue a x11hotplug event in a (patched) X-server\n"
	"Usage: " NAME " [OPTIONS] <DEVICEPATH> [OPT=VALUE ...]\n"
	"\n"
	"Options:\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -d, --display=DISPLAY	Set display, only DISPLAYs on localhost allowed\n"
	"\n"
	"Required keys\n"
	" type=(keyboard|pointer)	Input device type\n"
	" action=(add|change|del)	Hotplug action\n"
	" driver=(evdev|keybd|mouse)	X11 driver\n"
	" identifier=STRING		Device Identfifier\n"
	"\n"
	"Example:\n"
	" $ " NAME " -d :0 /dev/input/event0\n"
	;

#ifdef _GNU_SOURCE
static const struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },
	{ "display", required_argument, NULL, 'd', },
	{ },
};

#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif

static const char optstring[] = "?Vvsd:a:t:";

static struct args {
	int verbose;
	const char *display;
	int flags;
		#define FL_TYPE		0x01
		#define FL_ACTION	0x02
		#define FL_DRIVER	0x04
		#define FL_IDENTIFIER	0x08
} s = {
	.display = ":0",
};

static char buf[1024];

static void timeout(int sig)
{
	fprintf(stderr, "%s: timeout\n", NAME);
	exit(1);
}

/* return the remainder of @str if it started with @key */
static inline const char *strremainder(const char *key, const char *str)
{
	int len = strlen(key);

	return !strncmp(key, str, len) ? str + len : NULL;
}

int main(int argc, char *argv[])
{
	int opt, ret, sock, failure;
	const char *devfile;
	char *str, *result;
	struct sockaddr_un name;
	unsigned int namelen;

	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) != -1)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\n", NAME, LOCALVERSION);
		return 0;
	case 'v':
		++s.verbose;
		break;
	case 'd':
		s.display = optarg;
		break;
	case '?':
	default:
		fputs(help_msg, stderr);
		exit(1);
		break;
	}
	if (!s.display)
		error(1, 0, "no display specified!");

	if (!argv[optind]) {
		fputs(help_msg, stderr);
		exit(1);
	}
	devfile = argv[optind++];

	str = buf;
	str += sprintf(str, "device=%s\n", devfile);

	/* process arguments */
	for (; optind < argc; ++optind) {
		str += sprintf(str, "%s\n", argv[optind]);
		/* remember some vital parameters */
		if (strremainder("type=", argv[optind]))
			s.flags |= FL_TYPE;
		if (strremainder("action=", argv[optind]))
			s.flags |= FL_ACTION;
		if (strremainder("driver=", argv[optind]))
			s.flags |= FL_DRIVER;
		if (strremainder("identifier=", argv[optind]))
			s.flags |= FL_IDENTIFIER;
	}
	/* provide defaults for vital parameters */
	if (!(s.flags & FL_TYPE))
		str += sprintf(str, "type=keyboard\n");
	if (!(s.flags & FL_ACTION))
		str += sprintf(str, "action=add\n");
	if (!(s.flags & FL_DRIVER))
		str += sprintf(str, "driver=evdev\n");
	if (!(s.flags & FL_IDENTIFIER))
		str += sprintf(str, "identifier=x11hotplug-%s\n", basename(devfile));

	/* create socket */
	sock = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sock < 0)
		error(1, errno, "socket unix dgram");
	/* construct bind address */
	name = (struct sockaddr_un){ .sun_family = AF_UNIX, }; /* erase */
	sprintf(name.sun_path, "@x11hotplug-%i", getpid());
	namelen = SUN_LEN(&name);
	if (name.sun_path[0] == '@')
		name.sun_path[0] = 0;
	/* assign address */
	if (bind(sock, (void *)&name, namelen) < 0)
		error(1, errno, "bind socket @%s", name.sun_path+1);

	/* construct peer address */
	name = (struct sockaddr_un){ .sun_family = AF_UNIX, }; /* erase */
	sprintf(name.sun_path, "@x11hotplug%s", s.display);
	namelen = SUN_LEN(&name);
	if (name.sun_path[0] == '@')
		name.sun_path[0] = 0;

	/* schedule timeout handler */
	alarm(1);
	signal(SIGALRM, timeout);

	/* send hotplug info */
	ret = sendto(sock, buf, str-buf, 0, (void *)&name, namelen);
	if (ret < 0)
		error(1, errno, "sendto @%s", name.sun_path+1);
	if (s.verbose) {
		fprintf(stderr, "x11hotplug packet sent ...\n");
		fflush(stderr);
	}

	alarm(5);
	ret = recv(sock, buf, sizeof(buf), 0);
	if (ret <= 0)
		error(1, errno, "recv");
	buf[ret] = 0;
	str = strstr(buf, "\nresult=");
	result = str ? str+8 : "INVALID";
	failure = !strcmp(result, "ok") ? 0 : 1;
	if (s.verbose) {
		fprintf(stderr, "=== Xorg answered ===\n");
		fputs(buf, stderr);
		fprintf(stderr, "\n===\n");
	}
	fprintf(stderr, "x11hotplug %s: %s\n", strrchr(devfile, '/')+1, result);
	return failure; /* 0 is ok */
}
